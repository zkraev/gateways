﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Helpers.Enums
{
    public enum DeviceStatus
    {
        Offline = 0,
        Online = 1
    }
}
