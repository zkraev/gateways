﻿using Models;
using Services.DTOs.Gateway;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.GatewayService
{
    public interface IGatewayService
    {
        Task<IEnumerable<Gateway>> GetAll();
        Task<Gateway> GetById(long Id);
        Task<Gateway> AddDevice(long gatewayId, long deviceId);
        Task<Gateway> RemoveDevice(long gatewayId, long deviceId);        
        Task<Gateway> Create(GatewayDTO gateway);
        Task<Gateway> Update(GatewayDTO gateway);
        Task<bool> Delete(long Íd);
        Task<bool> DeviceCountValidation(long? Id);
        Task<bool> DeviceContainsCheck(long gatewayId, long deviceId);
    }
}
