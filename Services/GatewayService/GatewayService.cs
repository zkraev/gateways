﻿using AutoMapper;
using Data;
using Microsoft.EntityFrameworkCore;
using Models;
using Services.DTOs.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.GatewayService
{
    public class GatewayService : IGatewayService
    {
        private readonly DataContext db;
        private readonly IMapper mapper;

        public GatewayService(DataContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<Gateway>> GetAll()
        {
            return await db.Gateways.Include(x => x.Devices).ToListAsync();
        }

        public async Task<Gateway> GetById(long Id)
        {
            return await db.Gateways.Include(x=>x.Devices).FirstAsync(x => x.Id == Id);
        }

        public async Task<Gateway> Create(GatewayDTO gateway)
        {
            var newGateway = await db.Gateways.AddAsync(mapper.Map<Gateway>(gateway));

            await db.SaveChangesAsync();

            return newGateway.Entity;
        }

        public async Task<Gateway> Update(GatewayDTO gateway)
        {
            var dbGateway = await db.Gateways.FirstAsync(x=> x.Id == gateway.Id);

            if (dbGateway == null)
            {
                return null;
            }

            dbGateway = mapper.Map(gateway,dbGateway);

            await db.SaveChangesAsync();

            return dbGateway;
        }

        public async Task<bool> Delete(long id)
        {
            var dbGateway = await db.Gateways.FirstAsync(x => x.Id == id);

            if (dbGateway == null)
            {
                return false;
            }

            db.Gateways.Remove(dbGateway);            

            await db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeviceCountValidation(long? Id)
        {
            if (Id == null)
                return false;

            var dbGateways = await GetById(Id.Value);

            if (dbGateways.Devices.Count == 10)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeviceContainsCheck(long gatewayId, long deviceId)
        {
            var device = await db.Devices.FirstAsync(x => x.Id == deviceId);

            if (device.GatewayId != gatewayId)
                 return false;
            
            return true;
        }

        public async Task<Gateway> AddDevice(long gatewayId, long deviceId)
        {
            var device = await db.Devices.FirstAsync(x => x.Id == deviceId);
            device.GatewayId = gatewayId;
            db.SaveChanges();

            return await db.Gateways.Include(x => x.Devices).FirstAsync(x => x.Id == gatewayId);
        }

        public async Task<Gateway> RemoveDevice(long gatewayId, long deviceId)
        {
            var device = await db.Devices.FirstAsync(x => x.Id == deviceId);
            device.GatewayId = null;
            db.SaveChanges();

            return await db.Gateways.Include(x => x.Devices).FirstAsync(x => x.Id == gatewayId);
        }
    }
}
