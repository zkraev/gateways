﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DTOs.Gateway
{
    public class GatewayDTO
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }
        public string IPv4 { get; set; }
    }
}
