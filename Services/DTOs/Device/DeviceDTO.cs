﻿using Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DTOs.Device
{
    public class DeviceDTO
    {
        public long Id { get; set; }
        public long UID { get; set; }
        public string Vendor { get; set; }
        public DateTime CreatedDate { get; set; }
        public DeviceStatus Status { get; set; }
        public long? GatewayId { get; set; }
    }
}
