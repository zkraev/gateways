﻿using Models;
using Services.DTOs.Device;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.DeviceService
{
    public interface IDeviceService
    {
        Task<IEnumerable<Device>> GetAll();
        Task<IEnumerable<Device>> GetAllByGatewayId(long Id);
        Task<Device> GetById(long Id);        
        Task<Device> Create(DeviceDTO gateway);
        Task<Device> Update(DeviceDTO gateway);
        Task<bool> Delete(long id);
    }
}
