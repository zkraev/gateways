﻿using AutoMapper;
using Data;
using Models;
using Services.DTOs.Device;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Services.DeviceService
{
    public class DeviceService : IDeviceService
    {
        private readonly DataContext db;
        private readonly IMapper mapper;

        public DeviceService(DataContext db, IMapper mapper)
        {
            this.db = db;
            this.mapper = mapper;
        }

        public async Task<Device> Create(DeviceDTO device)
        {
            var newDevice = await db.Devices.AddAsync(mapper.Map<Device>(device));

            await db.SaveChangesAsync();

            return newDevice.Entity;
        }

        public async Task<bool> Delete(long id)
        {
            var dbDevice = await db.Devices.FirstAsync(x => x.Id == id);

            if (dbDevice == null)
            {
                return false;
            }

            db.Devices.Remove(dbDevice);

            await db.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<Device>> GetAll()
        {
            return await db.Devices.ToListAsync();
        }

        public async Task<IEnumerable<Device>> GetAllByGatewayId(long Id)
        {
            return await db.Devices.Where(x => x.GatewayId == Id).ToListAsync();
        }

        public async Task<Device> GetById(long Id)
        {
            return await db.Devices.FirstAsync(x => x.Id == Id);
        }

        public async Task<Device> Update(DeviceDTO device)
        {
            var dbDevice = await db.Devices.FirstAsync(x => x.Id == device.Id);

            if (dbDevice == null)
            {
                return null;
            }

            dbDevice = mapper.Map(device, dbDevice);

            await db.SaveChangesAsync();

            return dbDevice;
        }
    }
}
