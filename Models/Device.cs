﻿using Helpers.Enums;
using Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models
{
    public class Device : Entity
    {
        public long UID { get; set; }
        public string Vendor { get; set; }
        public DateTime CreatedDate { get; set; }
        public DeviceStatus Status { get; set; }
        public long? GatewayId { get; set; }
        [ForeignKey("GatewayId")]
        public Gateway Gateway { get; set; }
    }
}
