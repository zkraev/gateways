﻿using Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Gateway : Entity
    {
        public string SerialNumber { get; set; }
        public string Name { get; set; }
        public string IPv4 { get; set; }
        public ICollection<Device> Devices { get; set; }
    }
}
