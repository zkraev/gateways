﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Gateways.ViewModels.Gateway;
using Microsoft.AspNetCore.Mvc;
using Services.DeviceService;
using Services.DTOs.Gateway;
using Services.GatewayService;

namespace Gateways.Controllers
{
    public class GatewayController : ApiController
    {
        private readonly IGatewayService gatewayService;
        private readonly IDeviceService deviceService;
        private readonly IMapper mapper;

        public GatewayController(IGatewayService gatewayService, IDeviceService deviceService, IMapper mapper)
        {
            this.gatewayService = gatewayService;
            this.mapper = mapper;
            this.deviceService = deviceService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<GatewayVM>>> GetAll()
        {
            var gateways = await gatewayService.GetAll();
            var gatewaysVM = mapper.Map<List<GatewayVM>>(gateways);

            return gatewaysVM;
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult<GatewayVM>> Get(long id)
        {
            var gateway = await gatewayService.GetById(id);

            if (gateway == null)
            {
                return BadRequest();
            }

            var gatewayVM = mapper.Map<GatewayVM>(gateway);

            return gatewayVM;
        }

        [HttpPost]
        public async Task<ActionResult<CreateUpdateGatewayVM>> Create([FromBody] CreateUpdateGatewayVM model)
        {
            var newGateway = mapper.Map<GatewayDTO>(model);

            var createdGateway = await gatewayService.Create(newGateway);

            if (createdGateway == null)
            {
                return BadRequest();
            }

            newGateway.Id = createdGateway.Id;

            return CreatedAtAction(nameof(Create), newGateway);
        }

        [HttpPut("{id:long}")]
        public async Task<ActionResult<CreateUpdateGatewayVM>> Update(long id, [FromBody] CreateUpdateGatewayVM model)
        {
            var gateway = mapper.Map<GatewayDTO>(model);
            gateway.Id = id;

            var updatedGateway = await gatewayService.Update(gateway);

            if (updatedGateway == null)
            {
                return BadRequest();
            }

            return mapper.Map<CreateUpdateGatewayVM>(updatedGateway);
        }

        [HttpDelete("{id:long}")]
        public async Task<ActionResult> Delete(long id)
        {
            var isDeleted = await gatewayService.Delete(id);

            if (!isDeleted)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPut]
        [Route("addDevice")]
        public async Task<ActionResult<GatewayVM>> AddDevice([FromBody] AddRemoveDeviceToGatewayVM model)
        {
            var gateway = await gatewayService.GetById(model.GatewayId);
            var device = await deviceService.GetById(model.DeviceId);

            if (gateway == null || device == null)
            {
                return BadRequest();
            }

            var deviceCountCheck = await gatewayService.DeviceCountValidation(model.GatewayId);

            if (deviceCountCheck)
            {
                ModelState.AddModelError("GatewayId", "Maximum count of devices per gateway reached.");

                return BadRequest(ModelState);
            }

            var result = await gatewayService.AddDevice(model.GatewayId, model.DeviceId);

            return mapper.Map<GatewayVM>(result);
        }

        [HttpPut]
        [Route("removeDevice")]
        public async Task<ActionResult<GatewayVM>> RemoveDevice([FromBody] AddRemoveDeviceToGatewayVM model)
        {
            var gateway = await gatewayService.GetById(model.GatewayId);
            var device = await deviceService.GetById(model.DeviceId);

            if (gateway == null || device == null)
            {
                return BadRequest();
            }

            var containsResult = await gatewayService.DeviceContainsCheck(model.GatewayId, model.DeviceId);
            
            if (!containsResult)
                return BadRequest();

            var result = await gatewayService.RemoveDevice(model.GatewayId, model.DeviceId);

            return mapper.Map<GatewayVM>(result);
        }

    }
}
