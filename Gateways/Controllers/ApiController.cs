﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Gateways.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiController : ControllerBase
    {
    }
}
