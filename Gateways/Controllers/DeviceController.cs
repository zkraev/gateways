﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Gateways.ViewModels.Device;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.DeviceService;
using Services.DTOs.Device;
using Services.GatewayService;

namespace Gateways.Controllers
{
    public class DeviceController : ApiController
    {
        private readonly IDeviceService deviceService;
        private readonly IGatewayService gatewayService;
        private readonly IMapper mapper;
                
        public DeviceController(IDeviceService deviceService, IGatewayService gatewayService, IMapper mapper)
        {
            this.deviceService = deviceService;
            this.gatewayService = gatewayService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceVM>>> GetAll()
        {
            var devices = await deviceService.GetAll();
            var devicesVM = mapper.Map<List<DeviceVM>>(devices);

            return devicesVM;
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult<DeviceVM>> Get(long id)
        {
            var device = await deviceService.GetById(id);

            if (device == null)
            {
                return BadRequest();
            }

            var deviceVM = mapper.Map<DeviceVM>(device);

            return deviceVM;
        }

        [HttpGet]
        [Route("bygateway/{id:long}")]
        public async Task<ActionResult<IEnumerable<DeviceVM>>> GetAllByGateway(long id)
        {
            var devices = await deviceService.GetAllByGatewayId(id);

            if (devices == null)
            {
                return BadRequest();
            }

            var devicesVM = mapper.Map<List<DeviceVM>>(devices);

            return devicesVM;
        }

        [HttpPost]
        public async Task<ActionResult<CreateUpdateDeviceVM>> Create([FromBody] CreateUpdateDeviceVM model)
        {
            var newDevice = mapper.Map<DeviceDTO>(model);

            var deviceCountCheck = await gatewayService.DeviceCountValidation(model.GatewayId);

            if (deviceCountCheck)
            {
                ModelState.AddModelError("GatewayId", "Maximum count of devices per gateway reached.");

                return BadRequest(ModelState);
            }            

            var createdDevice = await deviceService.Create(newDevice);

            if (createdDevice == null)
            {
                return BadRequest();
            }

            newDevice.Id = createdDevice.Id;

            return CreatedAtAction(nameof(Create), newDevice);
        }

        [HttpPut("{id:long}")]
        public async Task<ActionResult<CreateUpdateDeviceVM>> Update(long id, [FromBody] CreateUpdateDeviceVM model)
        {
            var device = mapper.Map<DeviceDTO>(model);
            device.Id = id;

            var updatedDevice = await deviceService.Update(device);

            if (updatedDevice == null)
            {
                return BadRequest();
            }

            return mapper.Map<CreateUpdateDeviceVM>(updatedDevice);
        }

        [HttpDelete("{id:long}")]
        public async Task<ActionResult> Delete(long id)
        {
            var isDeleted = await deviceService.Delete(id);

            if (!isDeleted)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}