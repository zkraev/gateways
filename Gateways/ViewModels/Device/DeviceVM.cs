﻿using Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateways.ViewModels.Device
{
    public class DeviceVM
    {
        public long Id { get; set; }
        public long UID { get; set; }
        public string Vendor { get; set; }
        public DateTime CreatedDate { get; set; }
        public DeviceStatus Status { get; set; }
        public long GatewayId { get; set; }
    }
}
