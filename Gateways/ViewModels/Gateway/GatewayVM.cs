﻿using Gateways.ViewModels.Device;
using System.Collections.Generic;

namespace Gateways.ViewModels.Gateway
{
    public class GatewayVM
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }        
        public string IPv4 { get; set; }
        public ICollection<DeviceVM> Devices { get; set; }
    }
}
