﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateways.ViewModels.Gateway
{
    public class AddRemoveDeviceToGatewayVM
    {
        public long DeviceId { get; set; }
        public long GatewayId { get; set; }
    }
}
