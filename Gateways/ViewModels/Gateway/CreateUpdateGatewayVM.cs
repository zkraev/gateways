﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gateways.ViewModels.Gateway
{
    public class CreateUpdateGatewayVM
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }

        [RegularExpression(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ErrorMessage ="Must be valid IP Address")]
        public string IPv4 { get; set; }
    }
}
