﻿using AutoMapper;
using Gateways.ViewModels.Device;
using Gateways.ViewModels.Gateway;
using Models;
using Services.DTOs.Device;
using Services.DTOs.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateways.Mappings
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Gateway
            CreateMap<Gateway, GatewayDTO>().ReverseMap();
            CreateMap<Gateway, GatewayVM>().ReverseMap();
            CreateMap<CreateUpdateGatewayVM, GatewayDTO>().ReverseMap();
            CreateMap<CreateUpdateGatewayVM, Gateway>().ReverseMap();

            //Device
            CreateMap<Device, DeviceDTO>().ReverseMap();
            CreateMap<Device, DeviceVM>().ReverseMap();
            CreateMap<CreateUpdateDeviceVM, DeviceDTO>().ReverseMap();
            CreateMap<CreateUpdateDeviceVM, Device>().ReverseMap();
        }
    }
}
